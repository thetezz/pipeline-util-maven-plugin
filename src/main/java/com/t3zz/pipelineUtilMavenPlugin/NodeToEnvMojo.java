package com.t3zz.pipelineUtilMavenPlugin;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


@Mojo(name = "nodeToEnv")
public class NodeToEnvMojo extends AbstractMojo {

	@Parameter(property="xpath") 
	private String xpath;
	
	@Parameter(property="fileName")
	private String fileName;

	//usage "mvn com.t3zz:pipeline-util-maven-plugin:nodeToEnv -Dxpath=<xpath to tag> [-DfileName=<xml file>]"
    public void execute() throws MojoExecutionException {
    	if (xpath == null){
    		throw new MojoExecutionException(
    				"propertyName needs to be defined.");
    	}
    	
		try {
			if ((fileName == null) || (fileName.equals("pom.xml")) || (fileName.endsWith("pom.xml"))) {
				writeNodeAsEnv("pom.xml", xpath);
			}
			else {
				if(!fileName.endsWith(".xml")) {
					throw new MojoExecutionException("Invalid file");
				}
				writeNodeAsEnv(fileName, xpath);
			}
			
		} catch (ParserConfigurationException e) {
			throw new MojoExecutionException(e.toString());
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		} catch (XPathExpressionException e) {
			throw new MojoExecutionException(e.toString());
		}
    }
    
    static void writeNodeAsEnv(String filePath, String xPathString) 
			throws ParserConfigurationException, SAXException, IOException, MojoExecutionException, XPathExpressionException{
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		Autobot autobot = new Autobot();
		Document document = autobot.parseXML(filePath);
		XPath xpath = XPathFactory.newInstance().newXPath();
		Node aNode =  (Node) xpath.evaluate(xPathString, document, XPathConstants.NODE);
		if (aNode == null) {
			throw new MojoExecutionException(
					"Could not evaluate xpath: " + xPathString );
		}	

		String nodeName = aNode.getNodeName();
		String nodeTextContent = aNode.getTextContent();
		nodeName = nodeName.toUpperCase();
		String fileName = nodeName + "_env.sh";
		fileWriter = new FileWriter(fileName);
		bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write("export " + nodeName + "=" + nodeTextContent);
		bufferedWriter.close();
		fileWriter.close();
	}
}
