package com.t3zz.pipelineUtilMavenPlugin;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.xml.sax.SAXException;

@Mojo(name = "incrementVersion")
public class IncrementVersionMojo extends AbstractMojo {
	//usage "mvn com.t3zz:pipeline-util-maven-plugin:incrementVersion -Dmode=[1-4]"
	
	//mode = 1 pertains to major version, mode 2..minor etc
	@Parameter(property="mode")
	private int mode;
	
	public void execute() throws MojoExecutionException, MojoFailureException {
		String fileName = "pom.xml";
		if (mode < 1 || mode > 4){
			throw new MojoExecutionException(
    				"mode must be 1-4.");
		}
	    try {
	    	PomIO.incrementVersion(fileName, mode);
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (ParserConfigurationException e){
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		} catch (TransformerException e) {
			throw new MojoExecutionException(e.toString());
		}
		
	}

}
