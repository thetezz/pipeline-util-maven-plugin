package com.t3zz.pipelineUtilMavenPlugin;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.xml.sax.SAXException;


@Mojo(name = "incrementVersionProperty")
public class IncrementVersionPropertyMojo extends AbstractMojo {
	//usage "mvn com.t3zz:pipeline-util-maven-plugin:incrementVersionProperty -DpropertyName=<property tag> -Dmode=[1-4]"

	@Parameter(property="propertyName")
	private String propertyName;
	
	//mode = 1 pertains to major version, mode 2..minor etc
	@Parameter(property="mode")
	private int mode;
	
	
    public void execute() throws MojoExecutionException { 
    	String fileName = "pom.xml";
    	if (propertyName == null || mode < 1 || mode > 4){
    		throw new MojoExecutionException(
    				"propertyName must be defined and mode must be 1-4.");
    	}
	    try {
	    	PomIO.incrementVersionProperty(fileName, propertyName, mode);
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (ParserConfigurationException e){
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		} catch (TransformerException e) {
			throw new MojoExecutionException(e.toString());
		}
    }
}
