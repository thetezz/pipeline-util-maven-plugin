package com.t3zz.pipelineUtilMavenPlugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;


@Mojo(name = "setProperty")
public class PropertySetterMojo extends AbstractMojo {
	//usage "mvn com.t3zz:pipeline-util-maven-plugin:setProperty -DpropertyName=watev -DpropertyValue=watev"

	@Parameter(property="propertyName")
	private String propertyName;
	@Parameter(property="propertyValue")
	private String propertyValue;
	
    public void execute() throws MojoExecutionException { 
    	String fileName = "pom.xml";
    	if (propertyName == null || propertyValue == null){
    		throw new MojoExecutionException(
    				"propertyName and propertyValue needs to be defined.");
    	}
    	/*  TODO Rewrite to implement Autobot class, marsh no longer implemented
	    try {
	    	PomIO.writeProperty(fileName, propertyName, propertyValue);
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
    	
    }
}
