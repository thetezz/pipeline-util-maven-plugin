package com.t3zz;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.maven.plugin.MojoExecutionException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class PomIO {
		
	static void writePropertyAsEnv(String filePath, String propertyName) 
			throws ParserConfigurationException, SAXException, IOException, MojoExecutionException{
		Boolean isPropertyFound = false;
		String propertyNameAsUpper = propertyName.toUpperCase();
		String fileName = propertyNameAsUpper + "_env.sh";
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		Autobot autobot = new Autobot();
		Document document = autobot.parsePom(filePath);
		Node properties = document.getElementsByTagName("properties").item(0);
		if (properties.getChildNodes() != null) {
			NodeList propertiesList = properties.getChildNodes();
			for (int i = 0; i < propertiesList.getLength(); i++) {
				Node aProperty = propertiesList.item(i);
				if (aProperty.getNodeName().equals(propertyName)) {
					String nodeContent = aProperty.getTextContent();
					fileWriter = new FileWriter(fileName);
					bufferedWriter = new BufferedWriter(fileWriter);
					bufferedWriter.write("export " + propertyNameAsUpper + "=" + nodeContent);
					bufferedWriter.close();
					fileWriter.close();
					
					isPropertyFound = true;
				}
			}
		}
		if (!isPropertyFound){
			throw new MojoExecutionException(
    				"version property: " + propertyName + " was not found." );
		}
	}
    
    static void writeVersionProperty(String filePath, String propertyName, String propertyValue) 
    		throws IOException, MojoExecutionException, TransformerException, ParserConfigurationException, SAXException{
        Boolean isPropertyFoundAndChanged = false;
        if(!Version.validateVersion(propertyValue)) {
        	throw new MojoExecutionException(
    				"version property: " + propertyValue + " invalid" );
        }
        Autobot autobot = new Autobot();
        Document document = autobot.parsePom(filePath);
        Node properties = document.getElementsByTagName("properties").item(0); 
    	if (properties.getChildNodes() != null){
    		NodeList propertiesList = properties.getChildNodes();
    		for (int i = 0; i < propertiesList.getLength(); i++) {
    			Node aProperty = propertiesList.item(i);
    			if (aProperty.getNodeName().equals(propertyName)  && 
    					!(aProperty.getTextContent().equals(propertyValue))){
    				aProperty.setTextContent(propertyValue);
    				isPropertyFoundAndChanged = true;
    			}
    		}
    	}	
    	if (isPropertyFoundAndChanged)
    		autobot.transform(document, filePath);
    	else
    		throw new MojoExecutionException(
    				"version property: " + propertyName + " was not found or changed." );
    }
    
    static void incrementVersionProperty(String filePath, String propertyName, int mode) 
    		throws IOException, MojoExecutionException, TransformerException, ParserConfigurationException, SAXException{
    	Boolean isPropertyFound = false;    	
    	Autobot autobot = new Autobot();
    	Document document = autobot.parsePom(filePath);
    	Node properties = document.getElementsByTagName("properties").item(0); 
    	if (properties.getChildNodes() != null){
    		NodeList propertiesList = properties.getChildNodes();
    		for (int i = 0; i < propertiesList.getLength(); i++) {
    			Node aProperty = propertiesList.item(i);
    			if (aProperty.getNodeName().equals(propertyName)){
    				String aString = aProperty.getTextContent();
    				aString = Version.incrementVersion(aString, mode);
    				if (aString == null){
    					throw new MojoExecutionException(
    							"invalid version found");
    				}
    				aProperty.setTextContent(aString);
    				isPropertyFound = true;
    			}
    		}
    	}	
    	if (isPropertyFound)
    		autobot.transform(document, filePath);
    	else
    		throw new MojoExecutionException(
    				"property: " + propertyName + " not found in " + filePath );
    }
    
    static void checkIsSnapshot(String filePath, Boolean isNegated)
    		throws ParserConfigurationException, SAXException, IOException, MojoExecutionException {
    	Autobot autobot = new Autobot();
    	Document document = autobot.parsePom(filePath);
    	Node aNode = document.getFirstChild();
    	NodeList nl = aNode.getChildNodes();
    	String version="";
    	for (int i=0; i< nl.getLength(); i++) {
    		aNode = (nl.item(i));
    		if (aNode.getNodeName().equals("version")) {
    				version=aNode.getTextContent();
    				break;
    		}
    	}
    	if(version.endsWith("SNAPSHOT") && isNegated==true) {
    		throw new MojoExecutionException(
					"SNAPSHOT qualifier found.");
    	}
    	if(!version.endsWith("SNAPSHOT") && isNegated ==false) {
    		throw new MojoExecutionException(
    				"SNAPSHOT qualifier not found.");
    	}   	
    }
}
