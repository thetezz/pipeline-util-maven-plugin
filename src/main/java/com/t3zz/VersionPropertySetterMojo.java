package com.t3zz;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import org.xml.sax.SAXException;

//similar to PropertySetterMojo, this class will validate that the property is a version string
@Mojo(name = "setVersionProperty")
public class VersionPropertySetterMojo extends AbstractMojo {
	//usage "mvn com.t3zz:pipeline-util-maven-plugin:setVersionProperty -DpropertyName=<version property name> -DpropertyValue=<version property value>"

	@Parameter(property="propertyName")
	private String propertyName;
	@Parameter(property="propertyValue")
	private String propertyValue;
	
    public void execute() throws MojoExecutionException { 
    	String fileName = "pom.xml";
    	if (propertyName == null || propertyValue == null){
    		throw new MojoExecutionException(
    				"propertyName and propertyValue needs to be defined.");
    	}
	    try {
	    	PomIO.writeVersionProperty(fileName, propertyName, propertyValue);
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		} catch (TransformerException e) {
			throw new MojoExecutionException(e.toString());
		} catch (ParserConfigurationException e) {
			throw new MojoExecutionException(e.toString());
		}
    }
}
