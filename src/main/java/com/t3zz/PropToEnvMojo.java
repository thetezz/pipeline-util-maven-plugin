package com.t3zz;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.xml.sax.SAXException;


@Mojo(name = "propToEnv")
public class PropToEnvMojo extends AbstractMojo {

	@Parameter(property="propertyName")
	private String propertyName;

	//usage "mvn com.t3zz:pipeline-util-maven-plugin:propToEnv -DpropertyName=<property tag>"
    public void execute() throws MojoExecutionException {
    	String fileName = "pom.xml";
    	if (propertyName == null){
    		throw new MojoExecutionException(
    				"propertyName needs to be defined.");
    	}
    	
		try {
			PomIO.writePropertyAsEnv(fileName, propertyName);
		} catch (ParserConfigurationException e) {
			throw new MojoExecutionException(e.toString());
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		}
    }
}
