package com.t3zz;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class Autobot {
	public Document parsePom(String path) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(path);
		return doc;
	}
	
	public void transform (Document document, String path) throws TransformerException, IOException{
		DOMSource xmlInput = new DOMSource(document);
		StreamResult xmlFileOutput = new StreamResult(path);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
    	Transformer transformer = transformerFactory.newTransformer();
    	Files.copy(Paths.get(path), Paths.get("BACKUP_"+path));
		transformer.transform(xmlInput, xmlFileOutput);
	}
	
	
}