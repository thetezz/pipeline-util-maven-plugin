package com.t3zz;

public class Version {	
	
	//returns false if string version is composed of more than 4 integer
	//delimited by either '-' or '.'.
	public static Boolean validateVersion(String version) {		
		String [] tokens = version.split("-|\\.");
		if (tokens.length > 4) {
			return false;
		}
		
		for (int i = 0; i < tokens.length; i++) {
			if (! Version.isNum(tokens[i])) {
				return false; 
			}
		}
		return true;
	}
	
	
	public static String incrementVersion(String version, int mode) {
		Character delimiter1 = null;
		Character delimiter2 =null;
		Character delimiter3 =null;
		String majorVersion = null;
		String minorVersion=null;
		String incrementalVersion=null;
		String buildNumber=null;
		String returnString;
		
		if (version.length() > 0) {
			for (int i = 0; i < version.length(); i++) {
				if(version.charAt(i) == '.' || version.charAt(i)== '-') {
                      
					if (delimiter1==null){
						delimiter1 = version.charAt(i);
					    continue;
					}
					if (delimiter2==null) {
						delimiter2 = version.charAt(i);
						continue;
					}
					if (delimiter3==null){
						delimiter3 = version.charAt(i);
						continue;
					}
					return null; //found too many delimiters
					
				}
			}
		}
			
		String [] tokens = version.split("-|\\.");
		for (int i = 0; i < tokens.length; i++) {
			if (Version.isNum(tokens[i])) {
				if (i == 0)
					majorVersion = tokens[i];
				if (i == 1)
					minorVersion = tokens[i];
				if (i == 2 )
					incrementalVersion = tokens[i];
				if (i == 3)
					buildNumber = tokens[i];
			}
			else {
				return null; //eventually throw exception
			}
		}
		
		if(mode ==1 && majorVersion != null){
			Integer x = Integer.valueOf(majorVersion);
			x++;
			majorVersion=(Integer.toString(x));
		}
		else if(mode == 2 && minorVersion != null){
			Integer x = Integer.valueOf(minorVersion);
			x++;
			minorVersion=(Integer.toString(x));
		}
		else if(mode == 3 && incrementalVersion != null){
			Integer x = Integer.valueOf(incrementalVersion);
			x++;
			incrementalVersion=(Integer.toString(x));
			
		}
		else if(mode == 4 && buildNumber!=null){
			Integer x = Integer.valueOf(buildNumber);
			x++;
			buildNumber=(Integer.toString(x));
		}
		else{
			System.out.println("mode - version mismatch");
			return null;
		}
		
		if (delimiter1 != null)
			returnString = majorVersion + delimiter1 + minorVersion; 	
		else
			return majorVersion;
		if (delimiter2 != null)
			returnString = returnString + delimiter2 + incrementalVersion;
		else
			return returnString;
		if (delimiter3 != null)
			returnString = returnString + delimiter3 + buildNumber;
		return returnString;		
	}	
	
	public static boolean isNum(String strNum) {
	    boolean ret = true;
	    try {
	        Double.parseDouble(strNum);
	    }catch (NumberFormatException e) {
	        ret = false;
	    }
	    return ret;
	}
}
