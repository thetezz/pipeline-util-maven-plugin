package com.t3zz;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.xml.sax.SAXException;

@Mojo(name = "checkIsSnapshot")
public class CheckIsSnapshotMojo extends AbstractMojo {
	//usage "mvn com.t3zz:pipeline-util-maven-plugin:checkIsSnapshot [-Dnegate=<true | false>]"

	/**
	 * Negating will effectively check if NOT snapshot. therefore the build fails if
	 * the version does not have a SNAPSHOT qualifier
	 */
	@Parameter(property="negate", required = true, defaultValue="false")
	private Boolean isNegated;
	
	public void execute() throws MojoExecutionException {
		String fileName = "pom.xml";
		try {
			PomIO.checkIsSnapshot(fileName, isNegated);
		} catch (ParserConfigurationException e) {
			throw new MojoExecutionException(e.toString());
		} catch (SAXException e) {
			throw new MojoExecutionException(e.toString());
		} catch (IOException e) {
			throw new MojoExecutionException(e.toString());
		}
	}
}
