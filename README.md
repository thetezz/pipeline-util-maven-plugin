installation: 
$ mvn install

usage:
$ mvn com.t3zz:pipeline-util-maven-plugin:checkIsSnapshot [-Dnegate=<true | false>]

$ mvn com.t3zz:pipeline-util-maven-plugin:incrementVersion -Dmode=[1-4]
        where mode = 1 pertains to major version, mode 2..minor etc

$ mvn com.t3zz:pipeline-util-maven-plugin:incrementVersionProperty -DpropertyName=<property tag> -Dmode=[1-4]
        where mode = 1 pertains to major version, mode 2..minor etc

$ mvn com.t3zz:pipeline-util-maven-plugin:nodeToEnv -Dxpath=<xpath to tag> [-DfileName=<xml file>]

$ mvn com.t3zz:pipeline-util-maven-plugin:setProperty -DpropertyName=<property name> -DpropertyValue=<property value>

$ mvn com.t3zz:pipeline-util-maven-plugin:setVersionProperty -DpropertyName=<version property name> -DpropertyValue=<version property value>


